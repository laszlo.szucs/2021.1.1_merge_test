# 2021.1.1_merge_test

This repository contains instructions for testing the 2021.1.1 branch merge to the main pipeline branch. The main aim is to guarantee that ALMA results are not affected by the new code.

List of test datasets (suggestion from the PLWG, see complete list at: [Pipe-dev copy of Cycle 8 benchmark spreadsheet](https://docs.google.com/spreadsheets/d/158-B-L3xyIaCtGSgZ3WbHcIOjxJjoAM0VjYYc-kuVec/edit#gid=228040691)):

| Project | SOUS | Recipe | Command to obtain raw data |
| ------- | ---- | ------ | -------------------------- |
| 2019.1.00678.S | uid://A001/X14d8/X3eb | hifa_calimage | asdmExportLight uid___A002_Xe6a684_X7c41 |
| 2019.1.01482.T | uid://A001/X1465/Xb48 | hifa_polcal   | asdmExportLight uid___A002_Xe59f51_X3f99<br /> asdmExportLight uid___A002_Xe59f51_X4812<br />asdmExportLight uid___A002_Xe59f51_X51b6 |
| E2E7.1.00078.S | uid://A001/X13d7/X15d | hifa_calimage | asdmExportLight uid___A002_Xdfdfa9_X6d0b |
| 2017.1.00670.S | uid://A001/X1284/X20e0| hifa_calimage | asdmExportLight uid___A002_Xca8fbf_X5733 |
| 2016.1.00314.S | uid://A001/X87d/X62e | hifa_calimage | asdmExportLight uid___A002_Xcbdb2a_X10512 |

To obtain the raw dataset on cvpost nodes, first initalise scripts with below command and the use commands listed in table:

```bash
source /lustre/naasc/sciops/comm/rindebet/pipeline/scripts/pipeline_env_main.sh
```

The tests to be run with `CASA 6.2.0-118` and Pipeline `main` and `PIPE-1125-migrate-casa6.1.3-vlass-single-epoch-continuum-imaging-code-to-main-and-casa6.2` branches.

### Environment setup

```bash
# Prepare casa
cd ~/casa
wget https://casa.nrao.edu/download/distro/casa/releaseprep/casa-6.2.0-118.tar.xz
tar xf casa-6.2.0-118.tar.xz
export PATH=~/casa/casa-6.2.0-118/casa//bin:${PATH}

# Install dependecies
pip3 install astropy bdsf

# Optionally install image comparison module
cd
git clone https://gitlab.nrao.edu/lszucs/image-comparison.git
pip3 install image-comparison/.

# Prepare pipeline
cd ~/pipeline_src
mkdir main 2021.1.1_merge_to_main
cd main
git clone https://open-bitbucket.nrao.edu/scm/pipe/pipeline.git
cd ../2021.1.1_merge_to_main
python3 setup.py buildmytasks -i
git clone https://open-bitbucket.nrao.edu/scm/pipe/pipeline.git
git checkout PIPE-1125-migrate-casa6.1.3-vlass-single-epoch-continuum-imaging-code-to-main-and-casa6.2
python3 setup.py buildmytasks -i
```

### Preparing work folder and run pipeline

```bash
# Work and rawdata folders
cd /lustre/cv/users/lszucs/
mkdir -p 2021.1.1_to_main/main/2017.1.00670.S/rawdata main/2017.1.00670.S/working

# Obtain rawdata
source /lustre/naasc/sciops/comm/rindebet/pipeline/scripts/pipeline_env_main.sh

cd 2021.1.1_to_main/main/2017.1.00670.S/rawdata
asdmExportLight uid___A002_Xca8fbf_X5733
```

Create batch script (time limit 2 days, using 9 processes and 1 node, ask for large amount of memory to get the complete node):

```bash
echo `#!/bin/bash

#PBS -N casa_6.2.0-118_main
#PBS -l walltime=02:00:00:00
#PBS -d /lustre/cv/users/lszucs/2021.1.1_to_main/main/2017.1.00670.S/working
#PBS -L tasks=1:lprocs=9
#PBS -V
#PBS -j oe
#PBS -m abe
#PBS -M lszucs@nrao.edu

CASAPATH=/users/lszucs/casa/casa-6.2.0-118/
SCIPIPE_HEURISTICS=/users/lszucs/pipeline_src/main/pipeline

xvfb-run -d $CASAPATH/mpicasa -machinefile $PBS_NODEFILE $CASAPATH/casa --nologger --log2term -c "pipeline.recipereducer.reduce(vis=['../rawdata/uid___A002_Xca8fbf_X5733'], procedure='procedure_hifa_calimage.xml')" >& main_pipe.log` > ../working/batch_script.sh
```

The working directory (`PBS -d`), `SCIPIPE_HEURISTICS`, your e-mail address for notification (`PBS -M`) and the name of the measurement set needs to be changed for other setups.

Submit and monitor script:
```bash
qsub ../working/batch_script.sh
qstat -u lszucs
```
Similar procedure is used to run the pipeline with `SCIPIPE_HEURISTICS=/users/lszucs/pipeline_src/2021.1.1_merge_to_main`.

### Compare output images

```python
import image_comparison

image_comparison.compare_images('/lustre/cv/users/lszucs/2021.1.1_to_main/main/2017.1.00670.S/rawdata main/2017.1.00670.S/working/<image_name>','/lustre/cv/users/lszucs/2021.1.1_to_main/2021.1.1/2017.1.00670.S/rawdata main/2017.1.00670.S/working/<image_name>', fit_gauss=False)
```

Where `<image_name>` should be replaced by the image or PSF to be compared. A message should indicate when the files are numerically identical.
