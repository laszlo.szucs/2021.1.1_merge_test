#!/bin/bash

#PBS -N casa_6.2.0-118_main
#PBS -l walltime=02:00:00:00
#PBS -d /lustre/cv/users/lszucs/2021.1.1_to_main/main/2017.1.00670.S/working
#PBS -L tasks=1:lprocs=9
#PBS -V
#PBS -j oe
#PBS -m abe
#PBS -M lszucs@nrao.edu

CASAPATH=/users/lszucs/casa/casa-6.2.0-118/
SCIPIPE_HEURISTICS=/users/lszucs/pipeline_src/main/pipeline/

xvfb-run -d $CASAPATH/bin/mpicasa -machinefile $PBS_NODEFILE $CASAPATH/bin/casa --nologger --log2term -c "pipeline.recipereducer.reduce(vis=['../rawdata/uid___A002_Xca8fbf_X5733'], procedure='procedure_hifa_calimage.xml')" >& main_pipe.log
